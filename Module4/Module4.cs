﻿using System;
using System.Linq;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
            //реализация методов
        }


        public int Task_1_A(int[] array)
        {
            try
            {
                return array.Max();
            }
            catch
            {
                throw new ArgumentNullException("array", "is empty");
            }
        }

        public int Task_1_B(int[] array)
        {
            try
            {
                return array.Min();
            }
            catch
            {
                throw new ArgumentNullException("array", "is empty");
            }
        }

        public int Task_1_C(int[] array)
        {
            try
            {
                if (array.Length == 0) throw new ArgumentNullException("array", "is empty");
                return array.Sum();
            }
            catch
            {
                throw new ArgumentNullException("array", "is empty");
            }
        }

        public int Task_1_D(int[] array)
        {
            try
            {
                return array.Max() - array.Min();
            }
            catch
            {
                throw new ArgumentNullException("array", "is empty");
            }
        }

        public void Task_1_E(int[] array)
        {
            try
            {
                int max = array.Max();
                int min = array.Min();
                for (int i = 0; i < array.Length; i++)
                {
                    if (i % 2 == 0) array[i] += max;
                    else array[i] -= min;
                }
            }
            catch
            {
                throw new ArgumentNullException("array", "is empty");
            }
        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            return a + b + c;
        }

        public string Task_2(string a, string b)
        {
            return a + b;
        }

        public int[] Task_2(int[] a, int[] b)
        {
            int znach;
            int[] more;
            int[] few;
            if (a.Length > b.Length)
            {
                znach = b.Length;
                more = a;
                few = b;
            }
            else
            {
                znach = a.Length;
                more = b;
                few = a;
            }
            for (int i = 0; i < znach; i++)
            {
                more[i] += few[i];
            }
            return more;
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0)
            {
                throw new ArgumentNullException("radius", "can't be negative");
            }
            length = Math.PI * radius*2;
            square = Math.PI * Math.Pow(radius, 2);
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            maxItem = array.Max();
            minItem = array.Min();
            sumOfItems = array.Sum();
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            return (numbers.Item1 + 10, numbers.Item2 + 10, numbers.Item3 + 10);
        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0)
            {
                throw new ArgumentNullException("radius", "can't be negative");
            }
            else return (Math.PI * radius * 2, Math.PI * Math.Pow(radius, 2));
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            return (array.Min(), array.Max(), array.Sum());
        }

        public void Task_5(int[] array)
        {
            if (array.Length == 0)
            {
                throw new ArgumentNullException("array", "is empty");
            }
            for (int i = 0; i < array.Length; i++)
                array[i] += 5;
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array.Length == 0)
            {
                throw new ArgumentNullException("array", "is empty");
            }
            if (direction == 0)
            {
                Array.Sort(array);
            }
            else
            {
                Array.Sort(array);
                Array.Reverse(array);
            }
        }        

        public  double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            return ZnachX(func, x1, x2, e);
        }

        static double ZnachX(Func<double, double> func, double a, double b, double e)
        {
            double x = (a + b) / 2;
            if (func(a) * func(x) < 0) b = x;
            else a = x;
            if (Math.Abs(a - b) >= e*2) return ZnachX(func, a, b, e);
            else return x;
        }
    }
}
